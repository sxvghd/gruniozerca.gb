INCLUDE "code/hardware.inc"

SECTION "HEARTS_VARS", ROM0

Lives EQU $C00C

SECTION "HEARTS_CODE", ROM0

initHearts:
	; init lives
	ld a, 7 ; 111 in binary - 3 lives
	ld [Lives], a
	; init hearts
	;ld a, $0F
	;ld [$9800], a
	;ld [$9801], a
	;ld [$9802], a
	ret

; IN USE
; hl
loseLife:
	; load last heart addr
	ld hl, $9802
	; check if heart set
	; really simple, so loop unrolled
	ld a, [hl] ; switch to hld
	sub $0E
	jr nz, .sub
	dec hl
	ld a, [hl]
	sub $0E
	jr nz, .sub
	dec hl
	ld a, [hl]
	sub $0E
	jr nz, .sub
	ret ; no lives at this point
.sub
	ld a, $0E
	ld [hl], a
	ret	
