INCLUDE "code/hardware.inc"

SECTION "CARROT_VARS", ROM0

CarrotFrameCounter EQU $C006
CarrotSpeed EQU $C007 ; (how many pixels per update) probably unneeded, leaving just in case
CarrotSpawnDelay EQU $C008
CarrotFrame2Counter EQU $C009
CarrotSecondCounter EQU $C00A
CarrotUpdateSpeed EQU $C00B ; (how many loops before update)

TempSpace1 EQU $C019
TempSpace2 EQU $C01A
TempSpace3 EQU $C01B

CarrotSprites EQU $FE40

CarrotDefaultSpeed EQU 1 ; probably unneeded, leaving just in case
CarrotDefaultUpdateSpeed EQU 3
CarrotDefaultSpawnDelay EQU 5

SECTION "CARROT_CODE", ROM0

; RET
; a - random x position
getGoodXPos:
	call random8
	ld a, [RandomSeedC]
	; check if less than 8
	cp 8 
	jr c, getGoodXPos
	; check if more than 148
	cp 149
	jr nc, getGoodXPos
	ret

; IN USE - b
initSingleCarrot:
	ld hl, CarrotSprites
	ld b, 0 ; zero out the counter
.loop
	; check counter
	ld a, b
	sub 5
	jr z, .end
	; check if the carrot under hl is active
	ld a, [hl]
	or 0
	jr z, .init
	; if yes, increment the addr and the counter
	ld a, l
	add 8
	ld l, a
	inc b
	jr .loop
.init
	; save counter
	ld a,b
	ld [TempSpace1], a
	; for now, hardcode y
	ld a, 14
	ld [hl], a
	; for now, hardcode x
	inc hl
	call getGoodXPos
	ld [hl], a
	ld [TempSpace2], a ; save for later
	; first sprite num
	inc hl
	; get random color from x pos
	ld b, a
	ld c, 9
	call modulo8
	; remove unncecessary bits
	res 0, a
	res 1, a
	add 18 ; add 18 to make it a carrot sprite
	ld [hl], a
	ld [TempSpace3], a ; save for later
	; second sprite
	inc hl
	inc hl
	; for now, hardcode y
	ld a, 14
	ld [hl], a
	; for now, hardcode x
	inc hl
	ld a, [TempSpace2]
	add 8
	ld [hl], a
	; second sprite
	inc hl
	ld a, [TempSpace3] ; load the carrot addr
	add 2
	ld [hl], a
.end
	ret
	
initCarrots:
	; set counters
	xor a
	ld [CarrotFrameCounter], a
	ld [CarrotFrame2Counter], a
	ld [CarrotSecondCounter], a
	; set delay
	ld a, CarrotDefaultSpawnDelay
	ld [CarrotSpawnDelay], a
	; set carrots speed
	ld a, CarrotDefaultSpeed
	ld [CarrotSpeed], a
	; set carrots update speed
	ld a, CarrotDefaultUpdateSpeed
	ld [CarrotUpdateSpeed], a
	; for now, setup one carrot
	call initSingleCarrot
	ret
; TODO: carrot disappears
; ARG
; hl - carrot address
; IN USE
; e,c,d
updateSingleCarrot:
	; load carrots speed
	ld a, [CarrotSpeed] ; probably unneeded, leaving just in case
	ld e, a
	; load carrots y, just to check if active
	ld a, [hl]
	or 0 ; test if it's active
	jr z, .inactive
	; the carrot is active, so check if we're colliding with the floor
	ld a, [hl] ; load y value
	cp 120 ; check if it already hit the ground
	jr nc, .killCarrot
	call checkCarrotGrunioCollision ; check if collides with grunio
	or 0
	jr nz, .deleteCarrot
	; increment y and leave
	ld a, [hl] ; load y value
	add e
	ld [hl], a
	; set the second sprite
	inc hl
	inc hl
	inc hl
	inc hl
	ld [hl], a
	; rewind counter
	ld a, l
	sub 4
	ld l, a
	ret
.killCarrot
	; remove life
	; temporarily backup hl, because loseLife uses it 
	ld a, l
	ldh [$FF80], a
	ld a, h
	ldh [$FF81], a
	call loseLife
	ldh a, [$FF81]
	ld h, a
	ldh a, [$FF80]
	ld l, a	
.deleteCarrot
	;clear out all the fields
	ld d, 0
	ld bc, $0008
	call memset
	; rewind counter
	ld a, l
	sub 8
	ld l, a
	ret
.inactive
	ret

; ARG
; hl - carrot address
; a - carrot's y (hey, it's there and the function will be used only in that one spot, so why not)
; RET
; a - result
checkCarrotGrunioCollision:
	; already checked if not past the floor, so check if within grunio's reach
	cp 104
	jr c, .end
	; backup b
	ld a, b
	ld [TempSpace1], a
	; backup c
	ld a, c
	ld [TempSpace2], a
	; load grunio x
	ld a, [$FE09]
	ld b, a
	; load x
	inc hl
	ld c, [hl]
	; grunio x - carrot x (if < 8 then collision)
	sub c 
	cp 8
	jr c, .collision
	; carrot x - grunio x (if < 8 then collision)
	ld a, c
	sub b
	cp 8 
	jr c, .collision
	jr .cleanup
.collision
; this is ugly (but it works)
; it's not like all of the code is pure shit
; it started being not that bad and then just went downhill from there
; like it always does
; i have given up at this point
; the damage is irreversible
; i just want to finish this 
; and forget
; but will i be able to do so?
	; restore b
	ld a, [TempSpace1]
	ld b, a
	; restore c
	ld a, [TempSpace2]
	ld c, a
	; restore carrot addr
	dec hl
	; leave result
	ld a, 1
	ret
.cleanup
	; restore b
	ld a, [TempSpace1]
	ld b, a
	; restore c
	ld a, [TempSpace2]
	ld c, a
	; restore carrot addr
	dec hl
.end
	; leave result
	xor a
	ret

handleCarrotCounters:
	ld a, [CarrotFrame2Counter]
	cp 60
	jr c, .skip
	xor a 
	ld [CarrotFrame2Counter], a
	ld a, [CarrotSecondCounter]
	inc a
	ld [CarrotSecondCounter], a
.skip
	inc a
	ld [CarrotFrame2Counter], a
	; second counter
	ld a, [CarrotSecondCounter]
	cp CarrotDefaultSpawnDelay
	jr c, .skip2
	xor a ; reset counter
	ld [CarrotSecondCounter], a
	call initSingleCarrot
.skip2
	ret
	

; IN USE - b
carrotsUpdate:
	; handle counters
	call handleCarrotCounters
	; check if we should do anything
	ld a, [CarrotUpdateSpeed]
	ld b, a
	ld a, [CarrotFrameCounter]
	cp b
	jr z, .careForCarrots
	; not doing anything, so just increment the counter and gtfo
	inc a
	ld [CarrotFrameCounter], a
	ret
.careForCarrots
	; reset the frame counter
	xor a
	ld [CarrotFrameCounter], a
	ld hl, CarrotSprites ; load table
	ld b, a ; set loop counter
.careLoop
	; handle loop counter
	ld a, b
	cp 5
	jr z, .end
	; update the carrot
	call updateSingleCarrot
	; increment the loop counter
	inc b
	; set the next carrot address
	ld a, l
	add 8 
	ld l, a
	jr .careLoop
.end
	ret
