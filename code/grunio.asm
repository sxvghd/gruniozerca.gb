INCLUDE "code/hardware.inc"

SECTION "GRUNIO_VARS", ROM0

GrunioX EQU $C000
GrunioY EQU $C001
GrunioAnimStat EQU $C002
GrunioAnimCount EQU $C003
GrunioFace EQU $C004
GrunioPressedA EQU $C005

SECTION "GRUNIO_CODE", ROM0

initGrunio:
	;set variables
	ld a, 0
	ld [GrunioFace], a
	ld [GrunioPressedA], a
	ld [GrunioAnimStat], a
	ld [GrunioAnimCount], a
	ld a, 40
	ld [GrunioX], a
	ld a, 120
	ld [GrunioY], a

	;butt
	;y pos
    ld [_OAMRAM], a
    ;x pos
	ld a, [GrunioX]
    ld [_OAMRAM+1], a
    ;tile num
    ld a, 0
    ld [_OAMRAM+2], a
    ;attributes
    ld a, 0
    ld [_OAMRAM+3], a
	;tummy
    ;y pos
    ld a, [GrunioY]
    ld [_OAMRAM+4], a
    ;x pos
	ld a, [GrunioX]
    add 8
	ld [_OAMRAM+1+4], a
    ;tile num
    ld a, 2
    ld [_OAMRAM+2+4], a
    ;attributes
    ld a, 0
    ld [_OAMRAM+3+4], a
	;head
    ;y pos
    ld a, [GrunioY]
    ld [_OAMRAM+8], a
    ;x pos
	ld a, [GrunioX]
    add 16
    ld [_OAMRAM+1+8], a
    ;tile num
    ld a, 8
    ld [_OAMRAM+2+8], a
    ;attributes
    ld a, 0
    ld [_OAMRAM+3+8], a

    ;set up sprite palette
    ld a, %11100100
    ld [rOBP0], a
	ret

; ARG
; - a - selection (32 - direction keys, 16 - button keys)
; RET
; - c - direction bits
getInput:
    ; select keys
	ld [rP1], a
    ; read buttons
    ld a, [rP1]
    ld a, [rP1]
    xor %11100000 ; get rid of the upper half (useless)
    ; save state in c
    ld c, a
    ret

; ARG
; - c - direction bits
animGrunio:
	ld a, c
	and 3 ; get left/right bits
	xor 3 ; check if any is pressed
	jr nz, .checkCounter
	; no press, so leave the default sprites
	ld a, 2
	ld [_OAMRAM+2+4], a
	ld d, 6
	ld a, [GrunioFace]
	add d
	ld [_OAMRAM+2+8], a
	ret
.checkCounter
	ld a, [GrunioAnimCount]
	cp 5 ; check if counter is at 5
	jr z, .animate ; if yes, swap sprites
	inc a ; if no, increment and return
	ld [GrunioAnimCount], a
	ret
.animate
	xor a ; zero out the counter
	ld [GrunioAnimCount], a
	ld a, [GrunioAnimStat]
	and a ; set flags
	jr z, .anim1
	xor a ; zero out the stat
	ld [GrunioAnimStat], a
	ld a, 2
	ld [_OAMRAM+2+4], a
	ld d, 8
	ld a, [GrunioFace]
	add d
	ld [_OAMRAM+2+8], a
	ret
.anim1
	inc a ; increment the stat
	ld [GrunioAnimStat], a
	ld a, 4
	ld [_OAMRAM+2+4], a
	ld d, 6
	ld a, [GrunioFace]
	add d
	ld [_OAMRAM+2+8], a
	ret	

; ARG
; - c - action bits
swapGrunio:
	ld a, c
	and 1 ; check if A button pressed
	jr z, .checkPressedA
	xor a 
	ld [GrunioPressedA], a ; save that A wasn't pressed 
	ret
.checkPressedA
	ld a, [GrunioPressedA]
	or 0 ; check if not pressed
	jr z, .swapFaceGrunio
	ret
.swapFaceGrunio
	ld a, 1
	ld [GrunioPressedA], a ; save that A is pressed
	ld a, [GrunioFace]
	cp 8 ; check if should start from the beginning
	jr z, .faceOverflow
	add 4
	jr .end
.faceOverflow
	xor a ; 0 
	jr .end
.end
	ld [GrunioFace], a ; save face value (i'm on a pun roll today)
	ret

; ARG
; - c - direction bits
moveGrunio:
	; check if right
    ld a, c ; load data
    and 1
    jr z, .moveRight
    ; check if left
    ld a, c ; reload data
    and 2
    jr z, .moveLeft
    ret
; move all 3 parts of the sprite
; TODO: maybe move to a separate func
.moveRight
	; move the butt
	ld a, [GrunioX]
    inc a
    ld [_OAMRAM+1], a
	; save new x position
	ld [GrunioX], a	
	; move the M E A T
    add 8 
    ld [_OAMRAM+1+4], a
	; move the head
    add 8
	ld [_OAMRAM+1+8], a
	; set attributes
	xor a
	ld [_OAMRAM+3], a
	ld [_OAMRAM+3+4], a
	ld [_OAMRAM+3+8], a
	ret
.moveLeft
	; move the butt
	ld a, [GrunioX]
    dec a
	ld [_OAMRAM+1+8], a
	; save new x position
	ld [GrunioX], a	
	; move the M E A T
    add 8 
    ld [_OAMRAM+1+4], a
	; move the head
    add 8
    ld [_OAMRAM+1], a
	; set attributes
	ld a, %00100000
	ld [_OAMRAM+3], a
	ld [_OAMRAM+3+4], a
	ld [_OAMRAM+3+8], a
	ret


grunioUpdate:
	ld a, 32
	call getInput
	call animGrunio
	call moveGrunio
	ld a, 16
	call getInput
	call swapGrunio
	ret	
