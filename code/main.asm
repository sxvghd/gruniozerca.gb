INCLUDE "code/hardware.inc"
INCLUDE "code/grunio.asm"
INCLUDE "code/carrot.asm"
INCLUDE "code/hearts.asm"
INCLUDE "code/tools.asm"

SECTION "IRQ_VBLANK", ROM0[$0040]
    jp main
	reti
SECTION "IRQ_LCDC", ROM0[$0048]
    reti
SECTION "IRQ_TIMER", ROM0[$0050]
    reti
SECTION "IRQ_SERIAL", ROM0[$0058]
    reti
SECTION "IRQ_JOYPAD", ROM0[$0060]
    reti

SECTION "ENTRY", ROM0[$100]
preInit: 
    di ;disable interrupts
    jp init
	
REPT $150 - $104
db 0
ENDR

SECTION "Main", ROM0
init:
	; turn off lcd
	xor a
	ld [rLCDC], a

	; turn off sound
	ld [rNR52], a
	
	call toolTest

	; load sprites to vram
	ld hl, $8000 ; VRAM addr
	ld de, sprites ; copy source addr
	ld bc, spritesEnd - sprites ; counter
	call memcpy
	
	; load background tiles to vram
	ld hl, $9000 ; VRAM addr
	ld de, mapTiles ; copy source addr
	ld bc, mapTilesEnd - mapTiles ; counter
	call memcpy

	; just clear the screen for now	
	;ld hl, $9800
	;ld bc, 1024
	;ld d, 40
	;call memset	
	
	; load background tiles to vram
	ld hl, $9800 ; VRAM addr
	ld de, mapMap ; copy source addr
	ld bc, mapMapEnd - mapMap ; counter
	call memcpy
	
	; oam ram too, so no weird glitchy sprites	
	ld hl, _OAMRAM
	ld bc, 160
	ld d, 0
	call memset	

	call initGrunio
	call initHearts
	call initCarrots
	
	;set up bg palette
	ld a, %11100100
	ld [rBGP], a
	
	;set up scroll
	xor a
	;y pos
	ld [rSCY], a
	;x pos
	ld [rSCX], a

	;set up LCD
	ld a, %11000111
	ld [rLCDC], a

	;enable interrupts
	ld a, %00000001
	ld [rIE], a
	ei

main:
	halt
	call grunioUpdate
	call carrotsUpdate
	reti

SECTION "GFX", ROM0
sprites:
INCBIN "sprites/grunio.bin", 0, 288
INCBIN "sprites/carrut.bin", 0, 192
spritesEnd:
mapTiles:
INCBIN "sprites/map.bin", 0, 224
INCBIN "sprites/hearts.bin", 0, 32
mapTilesEnd:
mapMap:
;INCBIN "sprites/map_map.bin", 0, 91
;db 5,5,5,5,5,5,5,5,5,5,5,9,5,5,5,5,5,5,5,5,0,0,0,0,0,0,0,0,0,0,0,0
;db 5,5,10,5,5,5,5,5,9,5,5,5,5,5,5,10,5,5,5,5,0,0,0,0,0,0,0,0,0,0,0,0
;db 5,5,5,5,5,5,6,5,5,5,10,5,10,5,10,5,5,10,7,5,0,0,0,0,0,0,0,0,0,0,0,0
;db 10,5,5,6,10,5,10,5,5,5,9,5,5,10,5,5,5,10,5,10,0,0,0,0,0,0,0,0,0,0,0,0
;db 5,5,5,5,5,5,5,5,5,5,5,5,5,9,5,6,5,5,9,5,0,0,0,0,0,0,0,0,0,0,0,0
DB $0F,$0F,$0F,$05,$05,$05,$05,$05,$05,$05
DB $05,$05,$09,$05,$05,$05,$05,$05,$05,$05
DB $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$05,$05,$0A,$05,$05,$05,$05,$05
DB $09,$05,$05,$05,$05,$05,$05,$0A,$05,$05
DB $05,$05,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$05,$05,$05,$05,$05,$05
DB $06,$05,$05,$05,$0A,$05,$0A,$05,$0A,$05
DB $05,$0A,$07,$05,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$00,$00,$0A,$05,$05,$06
DB $0A,$05,$0A,$05,$05,$05,$09,$05,$05,$0A
DB $05,$05,$05,$0A,$05,$0A,$00,$00,$00,$00
DB $00,$00,$00,$00,$00,$00,$00,$00,$05,$05
DB $05,$05,$05,$05,$05,$05,$05,$05,$05,$05
DB $05,$09,$05,$06,$05,$05,$09,$05,$00,$00
DB $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
DB $05,$05,$0A,$05,$05,$05,$05,$07,$05,$0A
DB $0A,$05,$05,$05,$05,$05,$05,$0B,$05,$0B
DB $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$05,$05,$05,$05,$09,$0A,$05,$05
DB $05,$05,$05,$09,$05,$0B,$05,$05,$05,$05
DB $05,$05,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$05,$05,$05,$05,$05,$05
DB $0A,$05,$0A,$05,$05,$05,$05,$05,$05,$08
DB $05,$05,$05,$0B,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$00,$00,$09,$05,$05,$0A
DB $05,$05,$05,$05,$05,$06,$05,$05,$05,$0B
DB $05,$05,$0B,$05,$05,$05,$00,$00,$00,$00
DB $00,$00,$00,$00,$00,$00,$00,$00,$05,$0A
DB $05,$06,$05,$0A,$05,$05,$05,$05,$05,$05
DB $08,$05,$0B,$05,$05,$05,$05,$09,$00,$00
DB $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
DB $05,$05,$05,$05,$05,$05,$05,$05,$0A,$05
DB $0A,$0A,$05,$05,$05,$06,$05,$05,$0A,$05
DB $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$05,$05,$05,$05,$05,$05,$08,$05
DB $05,$05,$05,$05,$05,$0A,$05,$05,$05,$05
DB $05,$05,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$05,$09,$05,$05,$0A,$05
DB $05,$05,$05,$0A,$05,$05,$06,$05,$0A,$05
DB $0A,$05,$05,$05,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$00,$00,$05,$05,$05,$05
DB $05,$05,$05,$0A,$05,$05,$05,$05,$05,$05
DB $05,$05,$05,$05,$05,$08,$00,$00,$00,$00
DB $00,$00,$00,$00,$00,$00,$00,$00,$05,$0C
DB $0C,$0C,$0C,$05,$0D,$05,$0C,$0C,$0C,$0C
DB $0D,$0D,$0D,$0C,$0C,$0C,$0C,$05,$00,$00
DB $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
DB $04,$04,$04,$04,$04,$04,$04,$04,$04,$04
DB $04,$04,$04,$04,$04,$04,$04,$04,$04,$04
DB $00,$00,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$01,$03,$01,$01,$01,$01,$00,$01
DB $01,$01,$02,$00,$01,$01,$01,$01,$01,$03
DB $01,$02,$00,$00,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$02,$01,$01,$01,$01,$03
DB $01,$01,$01,$01,$01,$01,$02,$01,$01,$01
DB $02,$01,$01,$01,$00,$00,$00,$00,$00,$00
DB $00,$00,$00,$00,$00,$00
mapMapEnd:

