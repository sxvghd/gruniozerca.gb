SECTION "TOOLS", ROM0

RandomSeedX EQU $C01B
RandomSeedA EQU $C01D
RandomSeedB EQU $C01E
RandomSeedC EQU $C01F

; ARG
; - hl - destination addr
; - de - source addr
; - bc - counter (how much to copy)
memcpy:
	; copy byte
	ld a, [de]
	ld [hl], a
	; process counters
	inc hl
	inc de
	dec bc
	; check
	ld a, b
	or c ; if either b or c have any 1's then the result won't be 0 and copying must continue
	jr nz, memcpy
	ret

; ARG
; - hl - destination addr
; - bc - counter (how much to fill)
; - d - byte to set
memset:
	; set byte
	ld a, d
	ld [hl], a
	; process counters
	inc hl
	dec bc
	; check
	ld a, b
	or c ; if either b or c have any 1's then the result won't be 0 and copying must continue
	jr nz, memset
	ret

; ARG
; b, c - factors
; RET
; a - result
multiply8:
	xor a ; clear a
.loop
	add b
	dec c
	jr nz, .loop
	ret	

; ARG 
; - b - dividend number
; - c - divisor number
; RET
; - a - modulo
modulo8:
	; check if divisor bigger or equal to the dividend
	ld a,b
	cp c
	jr c, .divTooBigEqualEnd
	jr z, .divTooBigEqualEnd
.checkLoop
	sub c ; substract divisor
	; check if the next substraction will roll over the counter
	cp c 
	jr nc, .checkLoop
	;if there's a roll over, then we already have the modulo
	ret
.divTooBigEqualEnd
	xor a
	ret

; IN USE
; b,c,d,e
; adapted from https://www.electro-tech-online.com/threads/ultra-fast-pseudorandom-number-generator-for-8-bit.124249/
random8:
	; load everything
	ld a, [RandomSeedX]
	ld b, a
	ld a, [RandomSeedB]
	ld d, a
	ld a, [RandomSeedC]
	ld e, a
	ld a, [RandomSeedA]
	ld c, a
	; algorithm
	inc b ; inc x
	; a = a^c^x (prev a already in a)
	xor e ; a^c
	xor b ; (a^c)^x
	ld c, a ; save
	; b = a+b (a already in a)
	add d
	ld d, a ; save
	; c = c + (b>>1)^a (b already in a)
	rrc a ; b >> 1
	xor c
	add e
	ld e, a ; save
	; save everything
	ld [RandomSeedC], a
	ld a, d
	ld [RandomSeedB], a
	ld a, c
	ld [RandomSeedA], a
	ld a, b
	ld [RandomSeedX], a
	ret 

; just a function to test the implementation of some of the tools
toolTest:
	; clear high ram
	ld hl, $FF80
	ld bc, $0010
	ld d, 0
	call memset
	
	call random8	
	call random8	
	ret 
