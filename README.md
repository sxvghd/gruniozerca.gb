# Gruniożerca

An incomplete port of [Gruniożerca](https://github.com/arhneu/gruniozerca) to the Nintendo Gameboy, written in gbz80 assembly, using the rgbds toolchain. 

Made mostly as a learning exercise, therefore the code is absolutely horrid, but I learnt something (like using registers as loop counters is a horrible idea) and whilst in it's unfinished state, the game actually works.

![](http://gopher.su/IMGopher/up/6157716822d37a648be04c3e1a12de29.bmp)